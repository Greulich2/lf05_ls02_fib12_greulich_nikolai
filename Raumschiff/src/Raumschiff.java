import java.util.ArrayList;

public class Raumschiff {

    private int PhotonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname="";

    private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

    public Raumschiff(
            int photonentorpedoAnzahl,
            int energieversorgungInProzent,
            int schildeInProzent,
            int huelleInProzent,
            int lebenserhaltungssystemeInProzent,
            int androidenAnzahl,
            String schiffsname,
            ArrayList<String> broadcastKommunikator,
            ArrayList<Ladung> ladungsverzeichnis)
    {
        this.PhotonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
        this.broadcastKommunikator = broadcastKommunikator;
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    public String toString() {
        return "\rRaumschiff:\n" +
                "\tName: " + schiffsname + "\n" +
                "\tEnergieversogung: " + energieversorgungInProzent + "\n" +
                "\tSchildzustand: " + schildeInProzent + "%\n" +
                "\tHüllenzustand: " + huelleInProzent + "%\n" +
                "\tZustand der Lebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + "%\n" +
                "\tAnzahl der Androiden: " + androidenAnzahl + "\n" +
                "\tBroadcast Kommunikator: " + broadcastKommunikator + "\n" +
                "\tLadungsverzeichnis: " + ladungsverzeichnis + "\n" +
        		"\tPhotonentorpedo Anzahl: " + PhotonentorpedoAnzahl + "\n";

    }

    public void ausgabeRaumschiff() {

        return ;
    }
    public int getPhotonentorpedoAnzahl() {
        return PhotonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        PhotonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public ArrayList<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
        this.broadcastKommunikator = broadcastKommunikator;
    }

    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }
    
    
    public void zustandRaumschiff() {
        System.out.println("Raumschiff: " +
                " name = " + schiffsname +
                ", PhotonentorpedoAnzahl = " + PhotonentorpedoAnzahl +
                ", energieversorgungInProzent = " + energieversorgungInProzent +
                ", schildeInProzent = " + schildeInProzent +
                ", huelleInProzent = " + huelleInProzent +
                ", lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent +
                ", androidenAnzahl = " + androidenAnzahl);
    }
    
    public void PhotonentorpedoAbschießen(int PhotonentorpedoAnzahl) {
        if (PhotonentorpedoAnzahl == 0) {
            System.out.println("-=*Click*=-");
        } else {
            this.PhotonentorpedoAnzahl = PhotonentorpedoAnzahl - 1;
            System.out.println("Photonentorpedo abgeschossen");
            treffer(); {
            	
            }
        }
        }
        public void phaserkanoneAbschießen() {
            if (this.energieversorgungInProzent < 50) {
                System.out.println("-=*Click*=-");
            } else {
                System.out.println("Phaserkanone abgeschossen");
                treffer();
            }
        }

        public void treffer() {
            System.out.println(schiffsname + "wurde getroffen");
        }
        public void abschussTorpedos() {
            if (PhotonentorpedoAnzahl == 0) {
                System.out.println("-=*Click*=-");
            } else {
                PhotonentorpedoAnzahl = PhotonentorpedoAnzahl - 1;
                System.out.println("Photonen Torpedos abgeschossen");
            }
}
}